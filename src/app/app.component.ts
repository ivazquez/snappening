import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

import { pageTransition } from './animations';

@Component({
  selector: 'snp-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [pageTransition]
})
export class AppComponent {
  title = 'snappening';

  prepRouteState(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }
}
