import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ParticleEffectButtonModule } from 'angular-particle-effect-button';

import { GauntletComponent } from './gauntlet/gauntlet.component';
import { ThanosComponent } from './thanos/thanos.component';
import { CardComponent } from './card/card.component';
import { RegisterFormComponent } from './register-form/register-form.component';

@NgModule({
  declarations: [
    GauntletComponent,
    ThanosComponent,
    CardComponent,
    RegisterFormComponent
  ],
  exports: [
    GauntletComponent,
    ThanosComponent,
    CardComponent,
    RegisterFormComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ParticleEffectButtonModule,
    ReactiveFormsModule
  ],
})
export class SharedModule { }
