import { AngularFirestore } from '@angular/fire/firestore';
import { Component } from '@angular/core';
import {
  FormBuilder,
  Validators
} from '@angular/forms';
import { Router } from '@angular/router';


import { Avenger } from '../../static-pages/avenger-page/avenger.Avenger';

@Component({
  selector: 'snp-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent {
  public open: boolean;
  public registerForm = this._fb.group({
    name: ['', Validators.required],
  });

  private _heroes = [
    'ca',
    'to',
    'nf',
    'im',
    'bw',
    'th',
    'he',
    'am'
  ];

  constructor(private _fb: FormBuilder, private _afs: AngularFirestore, private _router: Router) {
    this.open = true;
  }

  onSubmit() {
    if (this.registerForm.valid) {
      this.open = false;
      this.registerForm.disable();
      const selectedHero =  this._heroes[Math.floor(Math.random() * this._heroes.length)];
      const id = this._afs.createId();
      const newPlayer = Object.assign({}, this.registerForm.value, {active: true, hero: selectedHero });
      this._afs
        .collection<Avenger>('avengers')
        .doc(id)
        .set(newPlayer)
        .then(algo => this._router.navigate(['/avenger', id]));
    }
  }

}
