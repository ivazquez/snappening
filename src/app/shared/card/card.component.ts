import {
  Component,
  Input
} from '@angular/core';

@Component({
  selector: 'snp-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent {
  @Input() public hero: string;
  @Input() public name: string;
  @Input() public hidden: boolean;

  constructor() {
    this.hero = 'ca';
    this.name = '';
    this.hidden = false;
  }
}
