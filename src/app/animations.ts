import {
    trigger,
    stagger,
    animate,
    style,
    group,
    query,
    transition,
    animateChild
  } from '@angular/animations';

// const query = (s, a, o = {optional: true}) => q(s, a, o);

export const pageTransition =
    trigger('routeAnimations', [
        transition('* => TitlePage', [
            query(':enter', [
                group([
                    query('.home-page__title', style({opacity: 0, transform: 'scale(.7)'}), {optional: true}),
                    query('.home-page__title', animate('4000ms 11000ms ease-in', style({ opacity: '1', transform: 'scale(1)'}))),
                    query('.home-page__gauntlet', style({opacity: 0, transform: 'scale(.7)'}), {optional: true}),
                    query('.home-page__gauntlet', animate('4000ms 11000ms ease-in', style({ opacity: '1', transform: 'scale(1)'}))),
                ])
            ], {optional: true}),
        ]),
        transition('* <=> *', [
            style({ position: 'relative' } ),
            query(':leave', animateChild(), {optional: true}),
            group([
                query(':enter', [
                    query('.container', style({opacity: 0}), {optional: true}),
                    query('.container', animate('8000ms 8000ms ease-in', style({ opacity: '1'})), {optional: true}),
                ], {optional: true}),
            ]),
            query(':enter', animateChild(), {optional: true}),
        ])
    ]);
