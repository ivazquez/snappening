import { AngularFirestore } from '@angular/fire/firestore';
import {
   Component,
   OnDestroy
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Avenger } from './avenger.Avenger';
// import { avengerPageTransition } from './avenger-page.animations';

@Component({
  selector: 'snp-avenger-page',
  templateUrl: './avenger-page.component.html',
  styleUrls: ['./avenger-page.component.scss'],
  // animations: [ avengerPageTransition ],
})
export class AvengerPageComponent implements OnDestroy {
  public avenger$: Observable<Avenger>;
  private _params: any;
  private _paramsSubscription: any;

  constructor(private _activatedRoute: ActivatedRoute, private _afs: AngularFirestore) {
    this._paramsSubscription = this._activatedRoute.params.subscribe((params: any) => this._params = params);
    this.avenger$ = this._afs.doc<Avenger>(`avengers/${this._params.playerId}`).valueChanges();
  }

  ngOnDestroy() {
    this._paramsSubscription.unsubscribe();
  }
}
