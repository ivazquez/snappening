export interface Avenger {
    id: string;
    name: string;
    hero: string;
    active: boolean;
    visible?: boolean;
}
