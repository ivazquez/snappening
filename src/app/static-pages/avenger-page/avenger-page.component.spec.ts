import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvengerPageComponent } from './avenger-page.component';

describe('PlayerPageComponent', () => {
  let component: AvengerPageComponent;
  let fixture: ComponentFixture<AvengerPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
