import {
    trigger,
    animate,
    style,
    query,
    transition,
  } from '@angular/animations';

// const query = (s, a, o = {optional: true}) => q(s, a, o);

export const avengerPageTransition =
    [
        trigger('cardAnimation', [
            transition('*=>*', [
                query(':enter', [
                    style({ opacity: 0 }),
                    animate('1s ease-in', style({ opacity: 1 }))
                ], {optional: true})
            ])
        ]),
    ];
