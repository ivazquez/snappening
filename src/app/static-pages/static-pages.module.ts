import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { StaticPagesRoutingModule } from './static-pages-routing.module';

import { AvengerPageComponent } from './avenger-page/avenger-page.component';
import { HomePageComponent } from './home-page/home-page.component';
import { PlayersPageComponent } from './players-page/players-page.component';
import { RegisterPageComponent } from './register-page/register-page.component';
import { SnapPageComponent } from './snap-page/snap-page.component';

@NgModule({
  declarations: [
    AvengerPageComponent,
    HomePageComponent,
    PlayersPageComponent,
    RegisterPageComponent,
    SnapPageComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    StaticPagesRoutingModule,
  ]
})
export class StaticPagesModule { }
