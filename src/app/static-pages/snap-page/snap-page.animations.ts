import {
    trigger,
    stagger,
    animate,
    state,
    style,
    query,
    transition,
  } from '@angular/animations';

// const query = (s, a, o = {optional: true}) => q(s, a, o);

export const snapPageTransition =
    [
        trigger('cardsAnimation', [
            transition('*=>*', [
                query(':enter', [
                    style({ opacity: 0 }),
                    stagger('200ms', animate('1s ease-in',
                        style({ opacity: 1 }))
                    )
                ], {optional: true})
            ])
        ]),
        trigger('buttonAnimation', [
            state('initial', style({
                opacity: 0
            })),
            state('final', style({
                opacity: 1
            })),
            transition('initial=>final', animate('3s ease-in'))
        ]),
    ];
