import { AngularFirestore } from '@angular/fire/firestore';
import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { map, delay } from 'rxjs/operators';
import { Avenger } from '../avenger-page/avenger.Avenger';
import { snapPageTransition } from './snap-page.animations';
@Component({
  selector: 'snp-snap-page',
  templateUrl: './snap-page.component.html',
  styleUrls: ['./snap-page.component.scss'],
  animations: [ snapPageTransition ],
})
export class SnapPageComponent {
  public avengers$: Observable<Avenger[]>;
  public state: string;
  public avengers: Avenger[];

  constructor(private _afs: AngularFirestore) {
    this.state = 'initial';

    this._afs.collection<Avenger>('avengers', ref => ref.where('active', '==', true))
      .snapshotChanges()
      .pipe(
        map(actions => actions
          .map(avenger => {
            const data = avenger.payload.doc.data() as Avenger;
            data.visible = true;
            const id = avenger.payload.doc.id;
            return { id, ...data };
          })
        ),
        delay(4000)
      ).subscribe(data => this.avengers = data);
    setTimeout(function() { this.state = 'final'; }.bind(this), 4000);
  }

  public snap(): void {
    const remainingAvengers = this.avengers.filter(avenger => avenger.active === true).length;
    const numberOfAvegersToKill = Math.floor(remainingAvengers / 2);
    const avengersToKill = this.getRandomAvengers(this.avengers, numberOfAvegersToKill);
    avengersToKill.forEach(avenger => {
      const avengerRef = this._afs.collection(`avengers`).doc(`${avenger.id}`);
      avengerRef.update({active: false});
    });
  }

  private getRandomAvengers(avengers: Avenger[], numberOfAvegersToKill: number) {
    const result = new Array(numberOfAvegersToKill);
    let len = avengers.length;
    const taken = new Array(len);

    if (numberOfAvegersToKill > len) {
      throw new RangeError('getRandomAvengers: more Avengers taken than available');
    }

    while (numberOfAvegersToKill--) {
        const x = Math.floor(Math.random() * len);
        result[numberOfAvegersToKill] = avengers[x in taken ? taken[x] : x];
        taken[x] = --len in taken ? taken[len] : len;
        this.avengers[x].visible = false;
    }

    return result;
  }
}
