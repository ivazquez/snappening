import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SnapPageComponent } from './snap-page.component';

describe('SnapPageComponent', () => {
  let component: SnapPageComponent;
  let fixture: ComponentFixture<SnapPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SnapPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SnapPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
