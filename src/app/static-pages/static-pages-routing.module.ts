import { NgModule } from '@angular/core';
import {
  RouterModule,
  Routes,
} from '@angular/router';

import { AvengerPageComponent } from './avenger-page/avenger-page.component';
import { HomePageComponent } from './home-page/home-page.component';
import { PlayersPageComponent } from './players-page/players-page.component';
import { RegisterPageComponent } from './register-page/register-page.component';
import { SnapPageComponent } from './snap-page/snap-page.component';

const routes: Routes = [
  {
    path: '',
    component: RegisterPageComponent,
    pathMatch: 'full',
    data: {
      animation: 'RegisterPage'
    }
  },
  {
    path: 'play',
    component: HomePageComponent,
    pathMatch: 'full',
    data: {
      animation: 'TitlePage'
    }
  },
  {
    path: 'snap',
    component: SnapPageComponent,
    pathMatch: 'full',
    data: {
      animation: 'SnapPage'
    }
  },
  {
    path: 'players',
    component: PlayersPageComponent,
    pathMatch: 'full',
    data: {
      animation: 'PlayersPage'
    }
  },
  {
    path: 'avenger/:playerId',
    pathMatch: 'full',
    component: AvengerPageComponent,
    data: {
      animation: 'PlayerPage'
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class StaticPagesRoutingModule { }
