import {
    trigger,
    stagger,
    animate,
    style,
    query,
    transition,
  } from '@angular/animations';

// const query = (s, a, o = {optional: true}) => q(s, a, o);

export const homePageTransition =
    [
        trigger('lettersAnimation', [
            transition('* <=> *', [
                query(':enter', [
                    style({ opacity: 0, transform: 'translateZ(100px) translateY(-15px) rotateX(15deg)' }),
                    stagger('200ms', animate('700ms ease-in',
                        style({ transform: 'translateZ(0px) translateY(0px) rotateX(0deg)', opacity: 1 }))
                    )
                ], {optional: true}),
                query(':leave', animate('200ms', style({ opacity: 0 })), {optional: true})
            ])
        ]),
    ];
