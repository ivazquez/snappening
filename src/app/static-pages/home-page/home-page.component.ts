import {
  Component,
  ElementRef,
  ViewChild
} from '@angular/core';
import { homePageTransition } from './home-page.animations';

@Component({
  selector: 'snp-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
  animations: [ homePageTransition ],
})
export class HomePageComponent {
  @ViewChild('titleAudio', {read: ElementRef}) titleAudio: ElementRef;

  constructor() {
    setTimeout(function() { this.state = 'final'; }.bind(this), 26000);
  }
}
