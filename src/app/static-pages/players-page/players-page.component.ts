import { AngularFirestore } from '@angular/fire/firestore';
import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { map, delay } from 'rxjs/operators';
import { Avenger } from '../avenger-page/avenger.Avenger';
import { playersPageTransition } from './players-page.animations';
@Component({
  selector: 'snp-players-page',
  templateUrl: './players-page.component.html',
  styleUrls: ['./players-page.component.scss'],
  animations: [ playersPageTransition ],
})
export class PlayersPageComponent {
  public avengers$: Observable<Avenger[]>;
  public avengers: Avenger[];

  constructor(private _afs: AngularFirestore) {
    this._afs.collection<Avenger>('avengers')
      .snapshotChanges()
      .pipe(
        map(actions => actions
          .map(avenger => {
            const data = avenger.payload.doc.data() as Avenger;
            const id = avenger.payload.doc.id;
            return { id, ...data };
          })
        ),
      ).subscribe(data => this.avengers = data);
  }
}
