import {
    trigger,
    stagger,
    animate,
    style,
    query,
    transition,
  } from '@angular/animations';

export const playersPageTransition =
    [
        trigger('listAnimation', [
            transition('*=>*', [
                query(':enter', [
                    style({ opacity: 0 }),
                    stagger('200ms', animate('1s ease-in',
                        style({ opacity: 1 }))
                    )
                ], {optional: true})
            ])
        ]),
    ];
